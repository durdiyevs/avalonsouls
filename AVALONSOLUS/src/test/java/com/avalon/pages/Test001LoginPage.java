package com.avalon.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.avalon.utilities.Driver;



public class Test001LoginPage  {


public Test001LoginPage() {

	PageFactory.initElements(Driver.getDriverReference(), this);

}
//Example 1
@FindBy(xpath = "//input[@id='login-user-email']")
public WebElement usernameField;
@FindBy(xpath = "//input[@id='login-user-password']")
public WebElement passwordField;

@FindBy(xpath = "//div[@class='form-group']//input[@name='commit']")
public WebElement signinButton;


//
@FindBy(xpath = "//a[@class='sidebar-navigation-item active']")
public WebElement dashboardButton;

public void loginBonsai(String username, String password) {
	usernameField.sendKeys(username);
	passwordField.sendKeys(password);
	signinButton.click();
}
}
