package com.avalon.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.avalon.utilities.Driver;

public class Test002DemoPage {
	public Test002DemoPage() {

		PageFactory.initElements(Driver.getDriverReference(), this);

	}

	@FindBy(xpath = "//input[@id='restapibox']")
	public WebElement apiButton;

	@FindBy(xpath = "//button[@id='demo']")
	public WebElement saveButton;

	public boolean isEnabledMethod(WebElement seleniumButton) {
		boolean result = saveButton.isEnabled();
		return result;

	}
//		try {
//			boolean element = Driver.getDriverReference().findElement(By.xpath(path)).isEnabled();
//			if (element == true) {
//				System.out.println("Element is Enabled! ");
//			} else {
//				System.out.println("Element is not Enabled! ");
//			}
//		} catch (Exception e) {
//			System.out.println("Element Not Found!");
//		}
//	}

	public boolean isSelectedMethod(WebElement locator) {
		boolean result = apiButton.isSelected();
		return result;
	}
//		try {
//			boolean element = Driver.getDriverReference().findElement(By.xpath(path)).isSelected();
//			if (element == true) {
//				System.out.println("Element is Selected ! ");
//			} else {
//				System.out.println("Element is not Selected ! ");
//			}
//		} catch (Exception e) {
//			System.out.println("Element Not Found!");
//		}
//	}

}