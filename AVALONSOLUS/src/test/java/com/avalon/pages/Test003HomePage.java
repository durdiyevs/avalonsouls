package com.avalon.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.avalon.utilities.Driver;


public class Test003HomePage {
	public Test003HomePage() {
		PageFactory.initElements(Driver.getDriverReference(), this);
	}

	// Example 1
	@FindBy(xpath = "//a[@id='nav-link-accountList']")
	@CacheLookup // to optimize the
	public WebElement menu;
	@FindBy(xpath = "//span[contains(text(),'Baby Registry')]")
	public WebElement babyRegistry;
	
	@FindBy(xpath = "//a[@id='a-autoid-2-announce']")
	public WebElement babyRegistryButton;

	
	public void mouseHoverMethod(WebElement newMenu, WebElement newSelect) {
		Actions act = new Actions(Driver.getDriverReference());
		try {
			Thread.sleep(1000);
			act.moveToElement(newMenu).click(newSelect).build().perform();
			System.out.println(Driver.getDriverReference().getTitle());
	
		} catch (InterruptedException e) {
			System.out.println("Program timed out before selecting! ");
			e.printStackTrace();
		}
	}
	

}