package com.avalon.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.avalon.utilities.Driver;

public class Test004HomePage {
	public Test004HomePage() {
		PageFactory.initElements(Driver.getDriverReference(), this);

	}

	@FindBy(xpath = "//*[@id=\"features-section\"]/div/a")
	public WebElement startFreeButton;

}
