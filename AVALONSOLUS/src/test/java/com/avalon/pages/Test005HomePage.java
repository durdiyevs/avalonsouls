package com.avalon.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.avalon.utilities.Driver;

public class Test005HomePage {
	public Test005HomePage() {
		PageFactory.initElements(Driver.getDriverReference(), this);

	}

	@FindBy(xpath = "//button[@id='dblClkBtn']")
	public WebElement doubleClickgenButton;

	public void alertAccept() {
		Alert alert = Driver.getDriverReference().switchTo().alert();
		System.out.println("Alert Text\n" + alert.getText());
		alert.accept();
	}
}
