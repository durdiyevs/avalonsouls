package com.avalon.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.avalon.utilities.Driver;


public class Test006DemoPage {
	public Test006DemoPage() {

		PageFactory.initElements(Driver.getDriverReference(), this);

	}

	@FindBy(xpath = "//*[@id='credit2']/a")
	public WebElement a;
	@FindBy(xpath = "//*[@id='bank']/li")
	public WebElement b;

	@FindBy(xpath = "//*[@id='credit1']/a")
	public WebElement a2;
	@FindBy(xpath = "//*[@id='loan']/li")
	public WebElement b2;

	@FindBy(xpath = "//*[@id='fourth']/a")
	public WebElement a3;
	@FindBy(xpath = "//*[@id='amt7']/li")
	public WebElement b3;

	@FindBy(xpath = "//*[@id='fourth']/a")
	public WebElement a4;
	@FindBy(xpath = "//*[@id='amt8']/li")
	public WebElement b4;

	public void dragAndDrop(WebElement one, WebElement two) {
		Actions act = new Actions(Driver.getDriverReference());
		act.dragAndDrop(one, two).build().perform();
	}
}