package com.avalon.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;

import com.avalon.utilities.Driver;


public class Test007HomePage {
	public Test007HomePage() {

		PageFactory.initElements(Driver.getDriverReference(), this);

	}public void switchTabs(String a, String b) {
		Driver.getDriverReference().get(a);
		System.out.println(Driver.getDriverReference().getTitle());
		Driver.getDriverReference().findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println(e);
		}
		Driver.getDriverReference().get(b);
		System.out.println(Driver.getDriverReference().getTitle());
		Driver.getDriverReference().findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "\t");

	}
}
