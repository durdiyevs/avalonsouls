package com.avalon.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.avalon.utilities.Driver;




public class Test008DemoPage {
	public Test008DemoPage() {

		PageFactory.initElements(Driver.getDriverReference(), this);
	}

	@FindBy(xpath = "//button[@class='btn btn-danger']")
	public WebElement alertButton;
	
	
public void alertAccept() {
    Driver.getDriverReference().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    Alert alert = Driver.getDriverReference().switchTo().alert();
    System.out.println(alert.getText());	
    alert.accept();
}
}