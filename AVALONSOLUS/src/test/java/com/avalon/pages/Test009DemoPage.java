package com.avalon.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.avalon.utilities.Driver;

public class Test009DemoPage {
	public Test009DemoPage() {

		PageFactory.initElements(Driver.getDriverReference(), this);

	}

	// Example 1
	@FindBy(xpath = "//input[@id='fname']")
	public WebElement firstName;
	@FindBy(xpath = "//input[@id='lname']")
	public WebElement lastName;

	//
	@FindBy(xpath = "//div[@class='form-group']//input[@name='commit']")
	public WebElement dashboardButton;

	public void loginW3(String username, String password) {
		try {
			firstName.sendKeys(username);
			lastName.sendKeys(password);
		} catch (Exception e) {

			System.out.println("Exception Happened" + e.getMessage());
		}

	}

	public void switchFrameWithName(String frameName) {
		try {
			Driver.getDriverReference().switchTo().frame(frameName);
			Thread.sleep(2000);
			System.out.println("You Switced the Frame!");
		} catch (InterruptedException e) {

			System.out.println("Exception happened ! " + e.getMessage());
		}
	}
}
