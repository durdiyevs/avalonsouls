package com.avalon.runners;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)

//To run it as a cucumber class, means run it with cucumber.junit and cucumber.java dependencies
//Main method(Java Application), with TestNG TestNG Test

@CucumberOptions(features = "src\\test\\resources\\Features", glue = { "com.avalon.stepdefinitions" },

plugin = { "pretty", "json:target/cucumber-reports/cucumber.json",
		"junit:target/cucumber-reports/Cucumber.xml"},monochrome=true)
public class CukesRunner {

}
