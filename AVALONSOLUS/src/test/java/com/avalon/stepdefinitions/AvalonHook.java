package com.avalon.stepdefinitions;

import io.cucumber.*;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;

import com.avalon.utilities.Driver;

import io.cucumber.core.gherkin.Scenario;

public class AvalonHook {

	static Scenario scenario;

	@Before

	public void setUpCucumberDriver() {
		Driver.startDriver();

	}

	@After

	public void endTest() {
		
		Driver.closeDriver();

//	@After
//	public void teardown(Scenario scenario) {
//	    if (scenario.isFailed()) {
//	        try {
//	            byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
//	            String testName = scenario.getName();
//	            scenario.embed(screenshot, "image/png");
//	            scenario.write(testName);
//	        } catch (WebDriverException wde) {
//	            System.err.println(wde.getMessage());
//	        } catch (ClassCastException cce) {
//	            cce.printStackTrace();}
//	        }
//	    

		Driver.closeDriver();
	
	
		
	}
}