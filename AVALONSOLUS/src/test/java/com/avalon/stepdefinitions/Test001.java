package com.avalon.stepdefinitions;

import org.junit.Assert;

import com.avalon.pages.Test001LoginPage;
import com.avalon.utilities.BrowseUtils;
import com.avalon.utilities.ConfigurationReader;
import com.avalon.utilities.Driver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Test001 extends AvalonHook {

	@Given("User navigates to hellobonsai")
	public void user_navigates_to_hellobonsai() {
		Driver.startDriver();
		Driver.getDriverReference().get(ConfigurationReader.getProperty("urlhellobonsai"));
	}

	@When("^User logs in using Username as (.+) and Password (.+)$")
	public void user_logs_in_using_username_as_and_password(String username, String password) {
		Test001LoginPage t1 = new Test001LoginPage();
		t1.loginBonsai(username, password);
		BrowseUtils.threadWait(3000);
	}

	@Then("Login should be successful")
	public void login_should_be_successful() {
		Test001LoginPage t1 = new Test001LoginPage();
		BrowseUtils.doubleClick(t1.dashboardButton);
		String actual = BrowseUtils.getText(t1.dashboardButton);
		Assert.assertEquals("Dashboard", actual);
		System.out.println("Success");
	}
}
