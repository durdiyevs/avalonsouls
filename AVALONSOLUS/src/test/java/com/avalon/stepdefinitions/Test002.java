package com.avalon.stepdefinitions;

import org.junit.Assert;

import com.avalon.pages.Test002DemoPage;
import com.avalon.utilities.BrowseUtils;
import com.avalon.utilities.ConfigurationReader;
import com.avalon.utilities.Driver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Test002 {
	@Given("I am on testdiary demo page")
	public void i_am_on_testdiary_demo_page() {
		Driver.startDriver();
		Driver.getDriverReference().get(ConfigurationReader.getProperty("urltestdiary"));
	}

	@Given("I see Rest Api Button")
	public void i_see_Rest_Api_Button() {

		Test002DemoPage t2 = new Test002DemoPage();
		BrowseUtils.getText(t2.apiButton);
	}

	@Given("I see Save button")
	public void i_see_Save_button() {
		Test002DemoPage t2 = new Test002DemoPage();
		BrowseUtils.getText(t2.saveButton);
	}

	@When("I check Rest api button")
	public void i_check_Rest_api_button() {
		Test002DemoPage t2 = new Test002DemoPage();
		BrowseUtils.click(t2.apiButton);
	}

	@Then("I should see Selenium button selected")
	public void i_should_see_Selenium_button_selected() {
		Test002DemoPage t2 = new Test002DemoPage();
		System.out.println("it is " + BrowseUtils.getText(t2.apiButton));
		Assert.assertEquals(true, t2.isSelectedMethod(t2.apiButton));
	}



	@Then("I should see save button is not enabled")
	public void i_should_see_save_button_is_not_enabled() {
		Test002DemoPage t2 = new Test002DemoPage();
		t2.isEnabledMethod(t2.saveButton);
		Assert.assertEquals(false, t2.isEnabledMethod(t2.saveButton));
	}

}
