package com.avalon.stepdefinitions;

import org.junit.Assert;

import com.avalon.pages.Test003HomePage;
import com.avalon.utilities.BrowseUtils;
import com.avalon.utilities.ConfigurationReader;
import com.avalon.utilities.Driver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class Test003 {
	@Given("^User is on home page$")
	public void user_is_on_home_page() {
		Driver.startDriver();
		Driver.getDriverReference().get(ConfigurationReader.getProperty("urlamazon"));
	}

	@When("User Mousehover to Account and Lists and click Baby Registry")
	public void user_Mousehover_to_Account_and_Lists_and_click_Baby_Registry() {
		Test003HomePage t3 = new Test003HomePage();
		t3.mouseHoverMethod(t3.menu, t3.babyRegistry);
	}

	@Then("User should see Create a new Baby Registry Button")
	public void user_should_see_Create_a_new_Baby_Registry_Button() {
		Test003HomePage t3 = new Test003HomePage();
		BrowseUtils.getText(t3.babyRegistryButton);
		Assert.assertEquals("Create a new Baby Registry", BrowseUtils.getText(t3.babyRegistryButton));
		
		System.out.println("Success");

	}

}
