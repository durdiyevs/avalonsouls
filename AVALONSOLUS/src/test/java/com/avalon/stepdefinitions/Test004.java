package com.avalon.stepdefinitions;



import org.junit.Assert;

import com.avalon.pages.Test004HomePage;
import com.avalon.utilities.BrowseUtils;
import com.avalon.utilities.ConfigurationReader;
import com.avalon.utilities.Driver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Test004 {
	@Given("User is on hellobonsai landing page")
	public void user_is_on_hellobonsai_landing_page() {
		Driver.startDriver();
		Driver.getDriverReference().get(ConfigurationReader.getProperty("urlhomepagehellobonsai"));
	}

	@When("User scroll down {int} pixel")
	public void user_scroll_down_pixel(Integer int1) {
		BrowseUtils.scrollDownWithJavaScriptExecutor();
	}

	@Then("User should see Start Free button")
	public void user_should_see_Start_Free_button() {
		Test004HomePage t4 = new Test004HomePage();
		String actual = BrowseUtils.getText(t4.startFreeButton);
		Assert.assertEquals("START FREE", actual);
	}

}
