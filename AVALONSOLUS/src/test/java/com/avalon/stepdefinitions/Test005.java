package com.avalon.stepdefinitions;

import com.avalon.pages.Test005HomePage;
import com.avalon.utilities.BrowseUtils;
import com.avalon.utilities.ConfigurationReader;
import com.avalon.utilities.Driver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Test005 {
	@Given("User is on demo page")
	public void user_is_on_demo_page() {
		Driver.startDriver();
		Driver.getDriverReference().get(ConfigurationReader.getProperty("urlartoftesting"));
		BrowseUtils.threadWait(3000);
	}

	@When("User double click the button")
	public void user_double_click_the_button() {
		Test005HomePage t5 = new Test005HomePage();
		BrowseUtils.doubleClick(t5.doubleClickgenButton);
	}

	@Then("User should see an alert")
	public void user_should_see_an_alert() {
		Test005HomePage t5 = new Test005HomePage();
		t5.alertAccept();
	}
}
