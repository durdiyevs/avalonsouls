package com.avalon.stepdefinitions;

import org.junit.Assert;

import com.avalon.pages.Test006DemoPage;
import com.avalon.utilities.BrowseUtils;
import com.avalon.utilities.ConfigurationReader;
import com.avalon.utilities.Driver;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class Test006 {
	@Given("User is on guru99 demo page")
	public void user_is_on_guru99_demo_page() {
		Driver.startDriver();
		Driver.getDriverReference().get(ConfigurationReader.getProperty("urlguru99"));
	}

	@When("User draganddrop the items to the fields")
	public void user_draganddrop_the_items_to_the_fields() {
		Test006DemoPage dd = new Test006DemoPage();
		dd.dragAndDrop(dd.a, dd.b);
		dd.dragAndDrop(dd.a2, dd.b2);
		dd.dragAndDrop(dd.a3, dd.b3);
		dd.dragAndDrop(dd.a4, dd.b4);
	}

	@Then("User should see dropped items on the field")
	public void user_should_see_dropped_items_on_the_field() {
		Test006DemoPage dd = new Test006DemoPage();
System.out.println(BrowseUtils.getText(dd.b));
System.out.println(BrowseUtils.getText(dd.b2));
System.out.println(BrowseUtils.getText(dd.b3));
System.out.println(BrowseUtils.getText(dd.b4));

Assert.assertEquals("BANK", BrowseUtils.getText(dd.b));
Assert.assertEquals("SALES", BrowseUtils.getText(dd.b2));
Assert.assertEquals("5000", BrowseUtils.getText(dd.b3));
Assert.assertEquals("5000", BrowseUtils.getText(dd.b4));


System.out.println("Success");
	}
}
