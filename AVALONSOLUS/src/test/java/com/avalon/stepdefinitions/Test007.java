package com.avalon.stepdefinitions;

import org.junit.Assert;

import com.avalon.pages.Test007HomePage;
import com.avalon.utilities.BrowseUtils;
import com.avalon.utilities.ConfigurationReader;
import com.avalon.utilities.Driver;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Test007 {
	
	
	@Given("User is on amazon home page")
	public void user_is_on_amazon_home_page() {
		Driver.startDriver();
		Driver.getDriverReference().get(ConfigurationReader.getProperty("urlamazon"));
	}

	

	@When("User switch to google")
	public void user_switch_to_google() {
		Test007HomePage t7 = new Test007HomePage();
		t7.switchTabs(ConfigurationReader.getProperty("urlamazon"), "https://www.google.com");
	}

	@Then("User should see be on google page")
	public void user_should_see_be_on_google_page() {
		
		Assert.assertEquals("Google", Driver.getDriverReference().getTitle());
	}

}
