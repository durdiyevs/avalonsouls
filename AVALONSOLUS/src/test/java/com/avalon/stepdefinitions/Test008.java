package com.avalon.stepdefinitions;

import com.avalon.pages.Test008DemoPage;
import com.avalon.utilities.BrowseUtils;
import com.avalon.utilities.ConfigurationReader;
import com.avalon.utilities.Driver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Test008 {
	@Given("User is on automationtesting demo page")
	public void user_is_on_automationtesting_demo_page() {
		Driver.startDriver();
		Driver.getDriverReference().get(ConfigurationReader.getProperty("urlautomationtesting"));
	}

	@When("User click an alertbox button")
	public void user_click_an_alertbox_button() {
		Test008DemoPage t8 = new Test008DemoPage();
		BrowseUtils.doubleClick(t8.alertButton);

	}

	@Then("User should see an alert with message")
	public void user_should_see_an_alert_with_message() {
		System.out.println("Sucess");
	}

}
