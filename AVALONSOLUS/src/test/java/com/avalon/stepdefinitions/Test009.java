package com.avalon.stepdefinitions;

import com.avalon.pages.Test001LoginPage;
import com.avalon.pages.Test009DemoPage;
import com.avalon.utilities.BrowseUtils;
import com.avalon.utilities.ConfigurationReader;
import com.avalon.utilities.Driver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Test009 {
	@Given("I am on w3school demo page")
	public void i_am_on_w3school_demo_page() {
		Driver.startDriver();
		Driver.getDriverReference().get(ConfigurationReader.getProperty("urlw3school"));
	}

	@When("I insert firstname and lastname")
	public void i_insert_firstname_and_lastname() {
		Test009DemoPage t9 = new Test009DemoPage();
		t9.loginW3("Code", "test");
		BrowseUtils.threadWait(3000);
	}

	@Then("I should get NoSuchElementException")
	public void i_should_get_NoSuchElementException() {
		System.out.println("Exception happened");
	}

	@When("I switch iframe")
	public void i_switch_iframe() {
		Test009DemoPage t9 = new Test009DemoPage();
		t9.switchFrameWithName("iframeResult");
		BrowseUtils.threadWait(3000);
	}

	@Then("i should be able to insert firstname and lastname")
	public void i_should_be_able_to_insert_firstname_and_lastname() {
		Test009DemoPage t9 = new Test009DemoPage();
		BrowseUtils.threadWait(3000);
		t9.loginW3("Code", "test");
	}

}
