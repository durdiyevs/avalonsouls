package com.avalon.utilities;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BrowseUtils {

	public static void threadWait(int sec) {

		try {

			Thread.sleep(sec);

		} catch (InterruptedException e) {

			e.printStackTrace();

		}

	}

	public static WebElement waitForClickabilityByWebElement(WebElement element, int timeOut) {

		WebDriverWait wait = new WebDriverWait(Driver.getDriverReference(), timeOut);

		return wait.until(ExpectedConditions.elementToBeClickable(element));

	}

	public static WebElement waitForClickabilityByLocator(By locator, int timeOutLocator) {

		WebDriverWait wait = new WebDriverWait(Driver.getDriverReference(), timeOutLocator);

		return wait.until(ExpectedConditions.elementToBeClickable(locator));

	}

	public static void clickWithJavaScriptExecutor(WebElement element) {

		JavascriptExecutor js = (JavascriptExecutor) Driver.getDriverReference();

		js.executeScript("argument[0].click();", element);

	}

	public static void sendKeysWithJavaScriptExecutor() {

		JavascriptExecutor js = (JavascriptExecutor) Driver.getDriverReference();

		js.executeScript("document.getElementById('some id').value='someValue';");

	}

	public static void scrollDownWithJavaScriptExecutor() {

		JavascriptExecutor js = (JavascriptExecutor) Driver.getDriverReference();
		js.executeScript("window.scrollBy(0,2500)");

	}
	
	public static void scrollDownToBottomWithJavaScriptExecutor() {

		JavascriptExecutor js = (JavascriptExecutor) Driver.getDriverReference();
		 js.executeScript("window.scrollTo(0, document.body.scrollHeight)");

	}

	public static void mouseHover(WebElement element) {

		Actions mhover = new Actions(Driver.getDriverReference());
		mhover.moveToElement(element).perform();
	}

	public static void doubleClick(WebElement element) {

		new Actions(Driver.getDriverReference()).doubleClick(element).build().perform();

	}

	public static WebElement selectRandomDropdowwn(Select select) {

		Random rndm = new Random();

		List<WebElement> dropList = select.getOptions();
		List<WebElement> weblist = select.getOptions();
		int optionIndex = 1 + rndm.nextInt(weblist.size() - 1);
		select.selectByIndex(optionIndex);
		return select.getFirstSelectedOption();

	}
	
	public static String getText(WebElement locator) {
		String text = locator.getText();
		return text;
	}

	public static void click(WebElement element) {

		new Actions(Driver.getDriverReference()).click();

	}

		
	
}
