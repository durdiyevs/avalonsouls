package com.avalon.utilities;

import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtil {
	XSSFWorkbook wbook;
	XSSFSheet sheet;

	public ExcelUtil(String excelPath) {

		try {
			FileInputStream fis = new FileInputStream(excelPath);

			wbook = new XSSFWorkbook(fis);

			sheet = wbook.getSheetAt(0);

		} catch (Exception e) {

			System.out.println(e.getMessage());

		}

	}

	public String getData(int sheetNumber, int rowNumber, int cellNumber) {

		sheet = wbook.getSheetAt(sheetNumber);

		String data = sheet.getRow(rowNumber).getCell(cellNumber).getStringCellValue();

		return data;
	}

}
