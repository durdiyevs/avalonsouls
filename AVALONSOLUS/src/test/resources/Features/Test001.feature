@smoke
Feature: Login Functionality Feature
  
  In order to ensure Login Functionality works,
  I want to run the cucumber test to verify it is working

  Scenario Outline: Login Functionality
    Given User navigates to hellobonsai
    When User logs in using Username as <username> and Password <password>
    Then Login should be successful

    Examples: 
      | username                 | password      |
      | service_dev@codistan.com | IBecameACoder |
